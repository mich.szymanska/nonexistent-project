from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField

from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Length

from app.models import User


class LoginForm(FlaskForm):
    username = StringField('Login', validators=[DataRequired('Pole jest wymagane.')])
    password = PasswordField('Hasło', validators=[DataRequired('Pole jest wymagane.')])
    remember_me = BooleanField('Zapamiętaj mnie')
    submit = SubmitField('Zaloguj')


class RegisterForm(FlaskForm):
    username = StringField('Login', validators=[DataRequired('Pole jest wymagane.')])
    email = StringField('E-mail', validators=[DataRequired('Pole jest wymagane'), Email()])
    password = PasswordField('Haśło', validators=[DataRequired('Pole jest wymagane')])
    password2 = PasswordField('Powtórz hasło', validators=[EqualTo('password'),
                                                           DataRequired('Pole jest wymagane')])
    submit = SubmitField('Zarejestruj')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Ta nazwa użytkownika jest już zajęta')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Ten adres e-mail jest już powiązany z innym kontem.')


class EditProfileForm(FlaskForm):
    username = StringField('Nazwa użytkownika', validators=[DataRequired()])
    about_me = TextAreaField('O mnie', validators=[Length(min=0, max=140)])
    submit = SubmitField('Zapisz zmiany')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=username.data).first()
            if user is not None:
                raise ValidationError('Ta nazwa użytkownika jest już zajęta')
